﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assignment1.aspx.cs" Inherits="Assignment1.Assignment1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <h2>Dog Day Care Service</h2>
                <label>Name</label>
                <asp:TextBox runat="server" ID="ownerName"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your name" ControlToValidate="ownerName" ID="validatorName"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label>Phone Number</label>
                <asp:TextBox ID="phoneNumber" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="validatorPhoneFormat" runat="server" ErrorMessage="Invalid Phone Number Format" ControlToValidate="phoneNumber" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$" ></asp:RegularExpressionValidator> 
                <%--Referenced from https://stackoverflow.com/questions/9732455/how-to-allow-only-integers-in-a-textbox--%>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your phone number" ControlToValidate="phoneNumber" ID="validatorPhoneNumber"></asp:RequiredFieldValidator>


            </div>
            <div>
                <label>Email</label>
                <asp:TextBox ID="email" runat="server" placeholder="e.g. johndoe@domain.com"></asp:TextBox>
                <asp:RegularExpressionValidator ID="emailFormatValidation" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="email" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
                <%--Referenced from https://stackoverflow.com/questions/182542/email-address-validation-for-asp-net--%>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your email." ControlToValidate="email" ID="validatorEmail"></asp:RequiredFieldValidator>

            </div>
            
            <div>
                <label>Name of your Dog</label>
                <asp:TextBox runat="server" ID="dogName"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter the name of your Dog" ControlToValidate="dogName" ID="validatorDogName"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label>Breed</label>
                <asp:TextBox runat="server" ID="dogBreed"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter the breed of your dog" ControlToValidate="dogBreed" ID="validatorDogBreed"></asp:RequiredFieldValidator>
            </div>

            <div>
                <label>Sex:</label>
                <asp:RadioButtonList ID="dogSex" runat="server">
                <asp:ListItem>Male</asp:ListItem>
                <asp:ListItem>Female</asp:ListItem>
                </asp:RadioButtonList>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please select the sex of your dog" ControlToValidate="dogSex" ID="validatorDogSex"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label>Drop-off Time</label>
                <asp:DropDownList runat="server" ID="DropOffTime">
                    <asp:ListItem Text="8AM" Value="1"></asp:ListItem>
                    <asp:ListItem Text="9AM" Value="2"></asp:ListItem>
                    <asp:ListItem Text="10AM" Value="3"></asp:ListItem>
                    <asp:ListItem Text="11AM" Value="4"></asp:ListItem>
                    <asp:ListItem Text="12PM" Value="4"></asp:ListItem>
                </asp:DropDownList>
            </div>

            <div>
                <label>How many hours will your dog be at this service for? (Please note you cannot use this service for anything less than 3 hours)</label>
                <br />
                <asp:TextBox runat="server" ID="hoursOfService"></asp:TextBox>
                <asp:RangeValidator runat="server" ControlToValidate="hoursOfService" Type="Integer" MinimumValue="3" MaximumValue="8" ErrorMessage="Please enter a value between 3 and 8"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please indicate the number of hours you will be using this service" ControlToValidate="hoursOfService" ID="validatorHoursOfService"></asp:RequiredFieldValidator>

            </div>

            <div>
                <p>I have read and agree to the terms and conditions for this service indicated <a href="https://imgur.com/gallery/AfgkjCf" target="_blank">here</a>.</p>
                <asp:checkBox id="termsAndConditions" text="Accept Terms" Runat="server" />
                <p>Would you like to receive promotions and other related information about this service?</p>
                <asp:checkBox id="receivePromo" text="Yes, I would like to receive information about this service" Runat="server" />
            </div>

            <div>
                <asp:Button runat="server" ID="myButton" Text="Submit"/>
            </div>


             <asp:ValidationSummary runat="server" id="validationSummary" DisplayMode="BulletList" HeaderText="Please ensure that you have entered the following fields:" />
            <%--Referenced from https://docs.microsoft.com/en-us/dotnet/api/system.web.ui.webcontrols.validationsummary?view=netframework-4.7.2--%>
        </div>
    </form>
</body>
</html>



